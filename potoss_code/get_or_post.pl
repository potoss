#!/usr/bin/perl

use strict;
use warnings;
use HTTP::Request::Common;
use LWP::Simple;

get_page("lwp_test");
post_page("lwp_test");

sub get_page {
    my $page_name = shift;
    die "need page_name" if ! $page_name;
    my $data = get("http://www.pageoftext.com/PH_plain&nm_page=$page_name");
    _write_file("pot_$page_name.txt", $data);
}

sub post_page {
    my $page_name = shift;
    die "need page_name" if ! $page_name;
    my $filename = "pot_$page_name.txt";
    die "file not found" if ! -e $filename;
    my $data = _read_file($filename);
    require LWP::UserAgent;
    my $ua = LWP::UserAgent->new;
    my $response = $ua->request(POST 'http://www.pageoftext.com/?$page_name', [PH_page_submit => 1, nm_page => $page_name, nm_text => $data]);

#    if ($response->is_success) {
#        print $response->content;
#    }
#    else {
#        print $response->error_as_HTML;
#    }

}

sub _read_file {
    my $filename = shift;
    open(my $fh, "<", $filename)
        || die "Cannot read from file $filename - $!";
    my @lines = <$fh>;
    close($fh)
        || die "could not close $filename after reading";
    return join("", @lines);
}

sub _write_file {
    my $filename = shift;
    my $data = shift;
    open(my $fh, ">", $filename)
        || die "Cannot write to file $filename - $!";
    print $fh $data;
    close($fh)
        || die "could not close $filename after writing";
}